const mongoose = require('mongoose');

const usuarioSchema = mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId, 
    nome: String, 
    email:String,
    senha: String,
    projetos: String
});

module.exports =  mongoose.model('usuario', usuarioSchema);