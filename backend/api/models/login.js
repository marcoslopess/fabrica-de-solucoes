const mongoose = require('mongoose');

const loginSchema = mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId, 
    email: String, 
    senha: String
    
});

module.exports =  mongoose.model('login', loginSchema);