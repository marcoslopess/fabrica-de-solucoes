const express = require("express");
const router = express.Router();
const Cadastro = require("../models/cadastro");
const mongoose = require("mongoose");
const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

function criptografar(senha) {
  const DADOS_CRIPTOGRAFAR = {
    algoritmo: "aes256",
    segredo: "chaves",
    codificacao: "utf8",
    tipo: "hex",
  };

  const cipher = crypto.createCipher(
    DADOS_CRIPTOGRAFAR.algoritmo,
    DADOS_CRIPTOGRAFAR.segredo
  );
  cipher.update(senha);
  return cipher.final(DADOS_CRIPTOGRAFAR.tipo);
}

//criando um cadastro
router.post("/", (req, res, next) => {
    

  const dir =  path.resolve(__dirname, "..", "..", "fabrica_dir",`${req.body.email}`);

//   Verifica se não existe
  if (!fs.existsSync(dir)) {
    //Efetua a criação do diretório
    fs.mkdir(dir, (err) => {
      if (err) {
        console.log("Deu ruim...");
        return;
      }
      console.log("Diretório criado! =)");
    });
  }

  const cadastro = new Cadastro({
    _id: new mongoose.Types.ObjectId(),
    nome: req.body.nome,
    email: req.body.email,
    senha: criptografar(req.body.senha),
  });

  console.log(cadastro);
  
  cadastro
    .save()
    .then((result) => {
      res.status(201).json({
        message: "POST Request para /cadastro",
        cadastroriado: cadastro,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

//listando varios cadastro
router.get("/all", (req, res, next) => {
  const cadastro = Cadastro.find({})
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "GET Request para /cadastro",
        cadastro: result,
      });
    })
    .catch((err) =>
      res.status(500).json({
        error: err,
      })
    );
});

//listando um cadastro por id
router.get("/unique/:cadastroId", (req, res, next) => {
  const id = req.params.cadastroId;
  Cadastro.findById(id)
    .exec()
    .then((doc) => {
      res.status(200).json(doc);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

//listando um cadastro por nome e senha
router.get("/usuario", (req, res, next) => {
  let nome = req.headers.nome;
  let senha = req.headers.senha;

  Cadastro.findOne({
    nome: nome,
    senha: senha,
  })
    .then((doc) => {
      if (doc === null) {
        res.status(500).json({ error: " Cadastro não existe" });
      }
      res.status(200).json(doc);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

//atualizando um cadastro
router.put("/:cadastroId", (req, res, next) => {
  const id = req.params.cadastroId;
  const cadastro = req.params.body;
  Cadastro.findByIdAndUpdate(id, req.body, { new: true })
    .exec()
    .then((doc) => {
      res.status(200).json({
        message: "Cadastro atualizado com sucesso.",
        cadastroAtualizado: doc,
      });
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

//deletando um cadastro
router.delete("/:cadastroId", (req, res, next) => {
  const id = req.params.cadastroId;
  Cadastro.deleteOne({ _id: id })
    .exec()
    .then((doc) => {
      res.status(200).json({ message: "Cadastro excluído com sucesso." });
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

module.exports = router;
