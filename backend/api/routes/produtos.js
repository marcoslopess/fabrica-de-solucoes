const express = require('express');
const router = express.Router();
const Produto = require('../models/produtos');
const mongoose = require('mongoose');

//localhost:3000/produtos
router.get('/', (req, res, next) =>{
    const produtos = Produto.find({})
    .exec()
    .then(result =>{
        res.status(200).json({
            message: 'GET Request para /produtos',
            produtos: result
            })
        }
    )
    .catch(err => res.status(500).json({
        error:err}
        ));
});

router.post('/', (req, res, next) =>{

    const produto = new Produto({
        _id: new mongoose.Types.ObjectId(),
        nome: req.body.nome,
        preco: req.body.preco
    });
    
    produto.save()
    .then(result => {
        res.status(201).json({
            message: 'POST Request para /produtos',
            produtoCriado: produto
        });
    })
    .catch(err =>{
        res.status(500).json({
            error: err
            
        });
    });
    
});

//manipulando  um unico produto localhost:3000/produtos/5e821c9c6492936f2a79f70e
router.get('/:produtoId', (req, res, next) =>{
    const id  = req.params.produtoId;
    Produto.findById(id)
    .exec()
    .then(doc => {
        res.status(200).json(doc);
    })
    .catch(err => {
        res.status(500).json({error:err});
    })
   
});

//deletando  um unico produto localhost:3000/produtos/5e821c9c6492936f2a79f70e
router.delete('/:produtoId', (req, res, next) =>{
    const id  = req.params.produtoId;
    Produto.deleteOne({_id:id})
    .exec()
    .then(doc => {
        res.status(200).json({message: "Produto excluído com sucesso."});
    })
    .catch(err => {
        res.status(500).json({error:err});
    })
   
});

//deletando  um unico produto localhost:3000/produtos/5e821c9c6492936f2a79f70e
router.put('/:produtoId', (req, res, next) =>{
    const id  = req.params.produtoId;
    const produto  = req.params.body;
    Produto.findByIdAndUpdate(id,req.body,{new:true})
    .exec()
    .then(doc => {
        res.status(200).json({
            message: "Produto atualizado com sucesso.",
            produtoAtualizado: doc
        });
    })
    .catch(err => {
        res.status(500).json({error:err});
    })
   
});

///TODO:
//Criar seu repositório no github, deixar esse repositório público.
//Criar uma conta no MongoDB Atlas e gerar um cluster.  ok
//gerar o código dos vídeos e rodar localmente ok
//Criar as rotas para recuperar todos os produtos (GET) 50% pronto
//Criar uma rota para deletar um produto  (DELETE) parecido com o GET - falta 
//Criar uma rota para atualizar um produto (PUT) - falta

module.exports = router;