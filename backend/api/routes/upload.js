const routes = require("express").Router();
const multer = require("multer");
const path = require("path");
const multerConfig = require("../../config/multer");
const mongoose = require("mongoose");
const Post = require("../models/Post");


routes.post(
  "/",
  multer({
    dest: (req, email, cb) => {
      let dir = path.resolve(
        __dirname,
        "..",
        "..",
        "fabrica_dir",
        `lopes@lopes.com`
      );
    },
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(
          null,
          path.resolve(__dirname, "..", "..", "fabrica_dir", "lopes@lopes.com")
        );
      },
      filename: (req, file, cb) => {
        const fileName = `${file.originalname}`;

        cb(null, fileName);
      },
    }),
  }).single("file", "email"),
  (req, res, next) => {
    console.log(req.headers.email);
    
    const post = new Post({
      name: req.file.originalname,
      size: req.file.size,
      key: req.file.filename,
      email: req.email,
      ur: "",
    });

    post
      .save()
      .then((result) => {
        res.status(201).json({
          message: "POST Request para /posts",
          postCriado: post,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  }
);

routes.get("/posts", (req, res) => {
  return res.json({ hello: "World" });
});

module.exports = routes;
