const multer = require("multer");
const path = require("path");
const crypto = require("crypto");

module.exports = {
  dest: (req, email, cb) => {
    let dir = path.resolve(__dirname, "..", "fabrica_dir", `${email}`);
    console.log(dir);
  },
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(
        null,
        path.resolve(__dirname, "..", "fabrica_dir", "marcos@marcos.com")
      );
    },
    filename: (req, file, cb) => {
      const fileName = `${file.originalname}`;

      cb(null, fileName);
    },
  }),
};
