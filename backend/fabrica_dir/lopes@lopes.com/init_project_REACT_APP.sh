#!/bin/bash -p 
## developer: Renan Moura - Arch - created: 11/04/2018


sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y
sudo apt --purge autoremove -y && sudo apt autoclean;
sudo apt-get install python-ttystatus python3-cliapp python-cliapp -y;

sudo apt-get install libcurl3 -y;
sudo apt-get install curl -y;

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install gcc g++ make -y
sudo apt-get install -y nodejs

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install -y yarn

sudo apt-get install -y nodejs 
sudo npm install -g npm@latest 
sudo npm install -g create-react-app 
sudo npm install -g react-scripts@latest

sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y; 
sudo apt --purge autoremove -y && sudo apt autoclean;

npx create-react-app gui_cube

cd ./gui_cube

sudo yarn install
sudo yarn add react react-app firebase antd axios
sudo yarn add -S react react-dom --save
sudo yarn add react-router-dom react-scripts
sudo yarn add kill-port redux react-redux
sudo yarn upgrade
yarn start -o --watch



















